﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using System.IO;

/// <summary>
/// 用于开始场景的控制类，挂接到Canvas上
/// </summary>
public class StartController : MonoBehaviour {

    public VideoPlayer video;
    public Slider level;
    public Slider nxn;

	void Start () {
    }
	
	public void OnClickStart()
    {
        LevelController.curLevel = (int)level.value;
        LevelController.curNXN = (int)nxn.value;
        UnityEngine.SceneManagement.SceneManager.LoadScene("JigsawScene");
    }
}
